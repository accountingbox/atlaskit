import { FabricChannel } from '@atlaskit/analytics-listeners';

export const analyticsEventKey = 'EDITOR_ANALYTICS_EVENT';
export const editorAnalyticsChannel = FabricChannel.editor;
