import '@babel/polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import MobileEditor from './mobile-editor-element';
import { IS_DEV } from '../utils';
import {
  createMentionProvider,
  createMediaProvider,
  createEmojiProvider,
  createCardClient,
  createCardProvider,
} from '../providers';
import { getModeValue, getQueryParams } from '../query-param-reader';

function main() {
  // Read default value from defaultValue query parameter when in development
  const rawDefaultValue = IS_DEV ? getQueryParams().get('defaultValue') : null;
  const defaultValue =
    IS_DEV && rawDefaultValue ? atob(rawDefaultValue) : undefined;

  ReactDOM.render(
    <MobileEditor
      mode={getModeValue()}
      cardClient={createCardClient()}
      cardProvider={createCardProvider()}
      defaultValue={defaultValue}
      emojiProvider={createEmojiProvider()}
      mediaProvider={createMediaProvider()}
      mentionProvider={createMentionProvider()}
    />,
    document.getElementById('editor'),
  );
}

window.addEventListener('DOMContentLoaded', main);
