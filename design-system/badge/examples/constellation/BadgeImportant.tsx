import React from 'react';

import Badge from '../../src';

export default () => <Badge appearance="important">{25}</Badge>;
