import React from 'react';

import Badge from '../../src';

export default () => (
  <Badge appearance="added" max={500}>
    {1000}
  </Badge>
);
