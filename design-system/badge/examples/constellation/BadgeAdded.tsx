import React from 'react';

import Badge from '../../src';

export default () => <Badge appearance="added">+100</Badge>;
