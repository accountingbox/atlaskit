export {
  Modal,
  Spotlight,
  SpotlightCard,
  SpotlightManager,
  SpotlightTarget,
  SpotlightTransition,
  modalButtonTheme,
  spotlightButtonTheme,
} from './components';

export { Pulse as SpotlightPulse } from './styled/Target';
