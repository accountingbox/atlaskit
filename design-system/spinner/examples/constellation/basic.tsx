import React from 'react';
import Spinner from '../../src';

const SpinnerExample = () => <Spinner />;

export default SpinnerExample;
