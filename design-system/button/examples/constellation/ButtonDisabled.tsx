import React from 'react';

import Button from '../../src';

export default () => <Button isDisabled>Disabled button</Button>;
