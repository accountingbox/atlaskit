import React from 'react';

import { DevPreviewWarning, md } from '@atlaskit/docs';

export default md`
  ${(
    <div style={{ marginTop: '0.5rem' }}>
      <DevPreviewWarning />
    </div>
  )}
  `;
