import React from 'react';

import Avatar from '../../src';

export default () => (
  <div>
    <Avatar size="large" />
    <Avatar size="large" appearance="square" />
  </div>
);
