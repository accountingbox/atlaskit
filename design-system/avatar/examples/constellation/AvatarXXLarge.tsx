import React from 'react';

import Avatar from '../../src';

export default () => (
  <div>
    <Avatar size="xxlarge" />
    <Avatar size="xxlarge" appearance="square" />
  </div>
);
