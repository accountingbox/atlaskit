import React from 'react';

import Avatar from '../../src';

export default () => (
  <Avatar
    name="Mike Cannon-Brookes"
    src="https://pbs.twimg.com/profile_images/568401563538841600/2eTVtXXO_400x400.jpeg"
    size="large"
  />
);
