import React from 'react';

import Avatar from '../../src';

export default () => (
  <div>
    <Avatar size="medium" />
    <Avatar size="medium" appearance="square" />
  </div>
);
