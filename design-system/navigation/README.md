# Navigation

**DEPRECATED** - This package is deprecated. We recommend using [@atlaskit/side-navigation](https://atlaskit.atlassian.com/packages/navigation/side-navigation) and [@atlaskit/atlassian-navigation](https://atlaskit.atlassian.com/packages/navigation/atlassian-navigation) instead.

This component is displayed as a sidebar and it contains two sections: "global" and "container". Both sections are used for navigating through different views and containers in a product.
